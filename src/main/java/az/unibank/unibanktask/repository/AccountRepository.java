package az.unibank.unibanktask.repository;

import az.unibank.unibanktask.entity.Account;
import az.unibank.unibanktask.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    List<Account> findAccountsByUserAndActiveIsTrue(User user);

    Optional<Account> findByAccountNumber(String accountNumber);

}