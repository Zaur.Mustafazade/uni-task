package az.unibank.unibanktask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnibankTaskApplication {


    public static void main(String[] args) {
        SpringApplication.run(UnibankTaskApplication.class, args);
    }


}
