package az.unibank.unibanktask.service;


import az.unibank.unibanktask.dto.request.UserRequest;
import az.unibank.unibanktask.entity.User;

public interface UserService {

    User findUserByPin(String pin);

    Long saveUser(UserRequest userRequest);

    User findById(Long id);

}