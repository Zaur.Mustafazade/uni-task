package az.unibank.unibanktask.service;


import az.unibank.unibanktask.dto.request.TransferRequest;
import az.unibank.unibanktask.dto.response.TransferResponse;

public interface TransferService {
    TransferResponse transfer(TransferRequest transferRequest);
}
