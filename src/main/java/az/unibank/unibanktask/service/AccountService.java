package az.unibank.unibanktask.service;


import az.unibank.unibanktask.dto.request.AccountRequest;
import az.unibank.unibanktask.dto.response.AccountResponse;
import az.unibank.unibanktask.entity.Account;

import java.util.List;

public interface AccountService {
    void saveAccount(AccountRequest accountRequest);

    List<AccountResponse> getAllByUserPinCode(String pinCode);

    void update(Account account);

    Account findAccountWhichTransferPossibleByAccountNumber(String accountNumber);

    Account findAccountByAccountNumber(String accountNumber);

}