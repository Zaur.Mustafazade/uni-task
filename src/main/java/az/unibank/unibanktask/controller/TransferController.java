package az.unibank.unibanktask.controller;

import az.unibank.unibanktask.dto.request.TransferRequest;
import az.unibank.unibanktask.dto.response.TransferResponse;
import az.unibank.unibanktask.service.TransferService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transfers")
@RequiredArgsConstructor
public class TransferController {

    private final TransferService transferService;
    @PostMapping
    public TransferResponse transfer(@RequestBody TransferRequest transferRequest) {
        return transferService.transfer(transferRequest);
    }

}